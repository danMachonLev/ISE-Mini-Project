package primitives;

/**
 * Ray - a foundational object in geometry 
 * the group of points on a straight line that are on one relative side
 * To a given point on the line called the beginning of the foundation. 
 * Defined by point and direction (unit vector) ...
 * 
 * @author
 */
public class Ray {
	/**
	 * point in 3d space tail
	 */
	private Point3D p0;
	/**
	 * direction of the ray head
	 */
	private Vector dir; 

	/**
	 * constructor that take in coordinates
	 * 
	 * @param Vector point 
	 */
	public Ray(Vector vec, Point3D point) {
		this.dir = vec;
		this.p0 = point;
	}
	
	/**
	 * overloads the equals method to compare two rays
	 * @param obj second ray
	 * @return return tru if obj are the same and false if they are not equal
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (!(obj instanceof Ray)) return false;
		Ray other = (Ray)obj;
		return this.p0.equals(other.p0) && this.dir.equals(other.dir);
	}

	/**
	 * print out the members of the ray
	 * @return return the string of the members
	 */
	@Override
	public String toString() {
		return "Point: " + this.p0.toString();
	}
}
