package geometries;

import primitives.Point3D;
import primitives.Vector;

/**
 * class Sphere is sphere in a 3d space
 * 
 * 
 * @author
 */
public class Sphere implements Geometry {
	/**
	 * center of the sphere
	 */
	private Point3D center;
	/**
	 * radius of the sphere
	 */
	private double radius;

	/**
	* constructor of a sphere
	* 
	* @param p0 point of sphere
	* @param r radius of the sphere
	*/
	public Sphere(Point3D p0, double r){
		this.center = p0;
		this.radius = r;
	}

	/**
	 * get center of the circle
	 * @return this point3d
	 */
	public Point3D getCenter() {
		return this.center;
	}

	/**
	 * get the radius of circle
	 * @return this radius 
	 */
	public double getRadius() {
		return this.radius;
	}
	/**
	 * get the normal of the sphere
	 * @param point point 3d
	 * @return return a normal 
	 */
	@Override
	public Vector getNormal(Point3D point) {
		// TODO Auto-generated method stub
		return null;	
	}

	/**
	 * get the values of the members in sphere
	 * @return return a string of the members 
	 */
	@Override
    public String toString()
    {
        return "Center: " + this.center.toString() + "\nRadius: " + this.radius;
    }
}
