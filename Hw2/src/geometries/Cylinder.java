package geometries;

import primitives.Point3D;
import primitives.Ray;
import primitives.Vector;

/**
 *  class Cylinder is cylinder in a 3d space
 * 
 * 
 * @author
 */
public class Cylinder extends Tube {
	/**
	 * height of the tube
	 */
	private double height;

	/**
	* constructor of a cylinder
	* 
	* @param a ray of the cylinder
	* @param r radius of the cylinder
	* @param h height of the cylinder
	*/
	public Cylinder(Ray a, double r, double h) {
		super(a,r);
		this.height = h;
	}

	/**
	 * get height of the cylinder
	 * @return this height
	 */
	public double getHeight() {
		return this.height;
	}

	/**
	 * get the normal of a cylinder
	 * @param point point 3d 
	 * @return return the normal
	 */
	@Override
	public Vector getNormal(Point3D point) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * get the values of the members in cylinder
	 * @return return a string of the members 
	 */
	@Override
	public String toString() {
		return  "Height" + this.height +"\nAxis Ray: " + this.axisRay.toString() + "\nRadius: " + this.radius;
	}
	
}
