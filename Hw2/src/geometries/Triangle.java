package geometries;

import primitives.Point3D;

/**
 * class Triangle is polygon with vertexes in a 3d space
 * 
 * 
 * @author
 */
public class Triangle extends Polygon {
	/**
	* constructor of a triangle
	* 
	* @param p1 point of triangle
	* @param p2 point of triangle
	* @param p3 point of triangle
	*/
	public Triangle(Point3D p1, Point3D p2, Point3D p3){
		super(p1, p2, p3);
	}

	/**
	 * get the values of the members in triangle
	 * @return return a string of the members 
	 */
	@Override
    public String toString()
    {
        return super.toString();
    }
}
