package geometries;

import primitives.Vector;
import primitives.Point3D;

/**
 * class plane is a Plane a point and vector 3d space 
 * 
 * 
 * @author
 */
public class Plane implements Geometry{
	/**
	 * point on the plane
	 */
	private Point3D q0;
	/**
	 * vector that is orthogonal to the plane 
	 */
	private Vector normal;

	/**
	* constructor of a plane with 3 points and a normal
	* 
	* @param p1 point of plane
	* @param p2 point of plane
	* @param p3 point of plane
	* @param v normal vector point
	*/
	public Plane(Point3D p1, Point3D p2, Point3D p3, Vector v){
		this.q0 = p1;
		this.normal = v;
	}

	/**
	* constructor of a plane with 3 points
	* 
	* @param p1 point of plane
	* @param p2 point of plane
	* @param p3 point of plane
	*/
	public Plane(Point3D p1, Point3D p2, Point3D p3){
		this.q0 = p1;
		
		Vector a = p2.subtract(p1);
		Vector b = p3.subtract(p1);
		this.normal = a.crossProduct(b);
	}

	/**
	 * get point 3d of the plane
	 * @return this point 3d
	 */
	public Point3D getPoint() {
		return this.q0;
	}

	
	/** Not sure if he wants a plain get normal for polygon
	 * get normal of the plane a vector that is orthogonal to the plane
	 * @return this normal
	 */
	 public Vector getNormal() {
	 	return this.normal;
	 }
	 
	/**
	 * get the normal of a plane
	 * @param point point 3d 
	 * @return return the normal
	 */
	@Override
	public Vector getNormal(Point3D point) {
		return this.normal;
	}

	/**
	 * get the values of the members in plane
	 * @return return a string of the members 
	 */
	@Override
	public String toString()
	{
		return "Point: " + this.q0.toString() + "\nNormal: " + this.normal.toString();
	}
}
