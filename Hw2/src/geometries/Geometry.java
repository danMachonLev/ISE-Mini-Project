package geometries;

import primitives.Point3D;
import primitives.Vector;

/**
 * 
 * 
 * 
 * @author
 */
public interface Geometry {
	/**
	* get the normal of a shape
	* 
	* @param p0 point 3d
	* @return return normalized vector at a point 3d 
	*/
	public Vector getNormal(Point3D p0);

}
