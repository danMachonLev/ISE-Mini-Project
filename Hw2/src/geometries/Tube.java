package geometries;

import primitives.Point3D;
import primitives.Ray;
import primitives.Vector;

/**
 * class tube is Tube in a 3d space
 * 
 * 
 * @author
 */
public class Tube implements Geometry {
	/**
	 * axis of the tube
	 */
	protected Ray axisRay;
	/**
	 * radius of the tube
	 */
	protected double radius;

	/**
	* constructor of a tube
	* 
	* @param a ray of the tube
	* @param r radius of the tube
	*/
	public Tube(Ray a, double r){
		this.axisRay = a;
		this.radius = r;
	}
	/**
	 * get the normal of a tube
	 * @param point point 3d 
	 * @return return the normal
	 */
	@Override
	public Vector getNormal(Point3D point) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * get the values of the members in tube
	 * @return return a string of the members 
	 */
	@Override
	public String toString()
	{
		return "Axis Ray: " + this.axisRay.toString() + "\nRadius: " + this.radius;
	}
}
