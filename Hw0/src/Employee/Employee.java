package Employee;
public abstract class Employee 
{
    protected String firstName;
    protected String lastName;
    protected int id;

    public Employee()
    {
        this.firstName="Plony";
        this.lastName="Almoni";
        this.id=0;
    }

    public Employee(String FN, String LN, int ID)
    {
        this.firstName=FN;
        this.lastName=LN;
        this.id=ID;
    }

    public void setFirstName(String FN)
    {
        this.firstName=FN;
    }

    public void setLastName(String LN)
    {
        this.lastName=LN;
    }

    public void setId(int ID)
    {
        this.id=ID;
    }

    public String getFirstName()
    {
        return this.firstName;
    }

    public String getLastName()
    {
        return this.lastName;
    }

    public int getId()
    {
        return id;
    }

    @Override
    public String toString()
    {
        return firstName + " " + lastName+" " + id;
    }

    public boolean equals (Employee e)
    {
        if(this.firstName!=e.firstName || this.lastName!=e.lastName || this.id!=e.id)
            return false;

        return true;
    }

    public abstract double earnings();
}