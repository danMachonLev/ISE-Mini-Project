package Employee;

public class CommissionEmployee extends Employee
{
    protected float grossSales;
    protected int commision;

    public CommissionEmployee()
    {
        super();

        this.grossSales = 0;
        this.commision = 0;
    }

    public CommissionEmployee (String FN, String LN, int ID, float GS, int C)
    {
        super(FN,LN,ID);

        try
        {
            if(GS<0 || C<0)
                throw new RuntimeException();
        }
        catch(RuntimeException e) 
        { 
            GS = 0;  
            C = 0;               
        } 


        this.grossSales = GS;
        this.commision = C;
    }

    public void setGrossSales(float GS)
    {
        try
        {
            if(GS<0)
                throw new RuntimeException();
        }
        catch(RuntimeException e) 
        { 
            GS = 0;                
        } 

        this.grossSales = GS;
    }

    public void setCommision(int C)
    {
        try
        {
            if(C<0)
                throw new RuntimeException();
        }
        catch(RuntimeException e) 
        { 
            C = 0;                
        } 

        this.commision = C;
    }

    public float getGrossSales()
    {
        return this.grossSales;
    }

    public int getCommision()
    {
        return this.commision;
    }

    @Override
    public String toString()
    {
        return firstName + " " + lastName+" " + id + " " + grossSales + " " + commision;
    }  

    public double earnings()
    {
        return (commision/100.0)*grossSales;
    }

    public boolean equals (CommissionEmployee e)
    {
        if(this.firstName!=e.firstName || this.lastName!=e.lastName || this.id!=e.id || this.commision!=e.commision || this.grossSales!=e.grossSales) 
            return false;

        return true;
    }
}