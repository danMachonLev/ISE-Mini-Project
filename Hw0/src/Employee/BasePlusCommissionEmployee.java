package Employee;

public class BasePlusCommissionEmployee extends CommissionEmployee
{
    protected float baseSalary;

    public BasePlusCommissionEmployee()
    {
        super();

        this.baseSalary = 0;
    }

    public BasePlusCommissionEmployee (String FN, String LN, int ID, float GS, int C, float BS)
    {
        super(FN,LN,ID,GS,C);

        try
        {
            if(BS < 0)
                throw new RuntimeException();
        }
        catch(RuntimeException e) 
        { 
            BS = 0;                
        } 


        this.baseSalary = BS;
    }

    public void setBaseSalary(float BS)
    {
        try
        {
            if(BS<0)
                throw new RuntimeException();
        }
        catch(RuntimeException e) 
        { 
            BS = 0;                
        } 

        this.baseSalary = BS;
    }

    public float getBaseSalary()
    {
        return this.baseSalary;
    }

    @Override
    public String toString()
    {
        return firstName + " " + lastName + " " + id + " " + grossSales + " " + commision + " " + baseSalary;
    }

    public boolean equals (BasePlusCommissionEmployee e)
    {
        if(this.firstName!=e.firstName || this.lastName!=e.lastName || this.id!=e.id || this.commision!=e.commision || this.grossSales!=e.grossSales || this.baseSalary!=e.baseSalary) 
            return false;

        return true;
    }

    public double earnings()
    {
        return super.earnings() + baseSalary;
    }
}