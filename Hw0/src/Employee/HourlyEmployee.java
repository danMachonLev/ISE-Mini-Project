package Employee;
public class HourlyEmployee extends Employee 
{
    protected int hours;
    protected float wage;

    public HourlyEmployee()
    {
        super();
    }

    public HourlyEmployee (String FN, String LN, int ID, int HRS, float W)
    {
        super(FN,LN,ID);

        try
        {
            if(HRS<0 || W<0)
                throw new RuntimeException();
        }
        catch(RuntimeException e) 
        { 
            HRS = 0;  
            W = 0;               
        } 


        this.hours = HRS;
        this.wage = W;
    }

    public void setHours(int HRS)
    {
        try
        {
            if(HRS<0)
                throw new RuntimeException();
        }
        catch(RuntimeException e) 
        { 
            HRS = 0;                
        } 

        this.hours = HRS;
    }

    public void setWage(float W)
    {
        try
        {
            if(W<0)
                throw new RuntimeException();
        }
        catch(RuntimeException e) 
        { 
            W = 0;                
        } 

        this.wage = W;
    }

    public int getHours()
    {
        return this.hours;
    }

    public float getWage()
    {
        return this.wage;
    }

    @Override
    public String toString()
    {
        return firstName + " " + lastName+" " + id + " " + hours + " " + wage;
    }

    public boolean equals (HourlyEmployee e)
    {
        if(this.firstName!=e.firstName || this.lastName!=e.lastName || this.id!=e.id || this.hours!=e.hours || this.wage!=e.wage) 
            return false;

        return true;
    }

    public double earnings()
    {
        return this.hours*this.wage;
    }
}
