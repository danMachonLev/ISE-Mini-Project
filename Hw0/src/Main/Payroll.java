package Main;


//Final HW0
import java.util.*;

import Employee.BasePlusCommissionEmployee;
import Employee.CommissionEmployee;
import Employee.Employee;
import Employee.HourlyEmployee;

    public class Payroll 
    {

    public static void main(String[] args) 
    {
        //Scanner in= new Scanner(System.in);

        ArrayList<Employee> arr = new ArrayList<Employee>();

        arr.add(new HourlyEmployee ("Eliana","Isaacson",123456789,52,32));

        arr.add(new CommissionEmployee ("Ashira","Isaacson",333333333,17,60));

        arr.add(new BasePlusCommissionEmployee("Talia", "Isaacson", 333333333, 17, 60, 800));

        for(Employee e : arr)
            {
                System.out.println(e);
                if(e instanceof BasePlusCommissionEmployee) 
                    System.out.println(e.earnings()*1.1);
                else
                    System.out.println(e.earnings());
            }

        //in.close();
    }
    
} 

